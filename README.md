collections
In this project we are going to learn how to use collection in java.
Collection is a Framework and it's a set of classes, interfaces. Collection are used in situations where data is dynamic.
The Java Collections API provide Java developers with a set of classes and interfaces that makes it easier to work with collections of objects, e.g. lists, maps, stacks etc.
The Java Collection interface (java.util.Collection) is one of the root interfaces of the Java Collection API.

Reduced development effort by using core collection classes rather than implementing our own collection classes.
Code quality is enhanced with the use of well tested collections framework classes.
Reduced effort for code maintenance by using collection classes shipped with JDK.
Reusability and Interoperability